use std::borrow::Cow;
use std::path::{Path, PathBuf};
use std::{env, fs, str};

use encre_css::Config;
use pochoir::{ComponentFile, Context};

fn copy_dir(src_path: &Path, target_path: &Path) {
    for src_file in fs::read_dir(src_path).unwrap().filter_map(|f| f.ok()) {
        let target_file = target_path.join(src_file.file_name());

        if src_file.path().is_dir() {
            fs::create_dir_all(&target_file).unwrap();
            copy_dir(&src_file.path(), &target_file)
        } else {
            fs::copy(src_file.path(), target_file).unwrap();
        }
    }
}

fn build_html(src_path: &Path, target_path: &Path, components_path: &Path, config: &Config) {
    for src_file in fs::read_dir(src_path).unwrap().filter_map(|f| f.ok()) {
        let target_file = target_path.join(src_file.file_name());

        if src_file.path().is_dir() {
            fs::create_dir_all(&target_file).unwrap();
            build_html(&src_file.path(), &target_file, components_path, config)
        } else if src_file.path().extension().map_or(false, |e| e == "html") {
            let html_content = build_single_html_file(&src_file.path(), &target_file, components_path);
            build_single_css_file(&html_content, &target_path.join("styles.css"), config);
        }
    }
}

fn build_single_html_file(src_path: &Path, target_path: &Path, components_path: &Path) -> String {
    match pochoir::compile(
        src_path.file_stem().unwrap().to_str().unwrap(),
        &mut Context::new(),
        |name| {
            if name == src_path.file_stem().unwrap().to_str().unwrap() {
                // Source for the page
                Ok(ComponentFile::new(
                    src_path.to_owned(),
                    fs::read_to_string(src_path).unwrap(),
                ))
            } else {
                let file_path = if name.ends_with(".html") {
                    Cow::Borrowed(name)
                } else {
                    Cow::Owned(format!("{name}.html"))
                };

                if let Ok(file_content) = fs::read_to_string(components_path.join(&*file_path)) {
                    // Components
                    Ok(ComponentFile::new(
                        components_path.join(&*file_path),
                        file_content,
                    ))
                } else {
                    Err(pochoir::component_not_found(name))
                }
            }
        },
    ) {
        Ok(result) => {
            fs::write(target_path, &result).expect("failed to write built file");
            result
        }
        Err(error) => {
            let source = fs::read_to_string(error.file_path()).unwrap_or_else(|e| {
                panic!("failed to read file `{}`: {e}", error.file_path().display())
            });

            panic!("{}", pochoir::error::display_ansi_error(&error, &source));
        }
    }
}

fn build_single_css_file(html_content: &str, target_css_file: &Path, config: &Config) {
    // Generate the css to the given output location
    let css = encre_css::generate([html_content], config);
    fs::write(target_css_file, css).expect("failed to write file");
}

fn main() {
    let manifest_dir: PathBuf = env::var("CARGO_MANIFEST_DIR")
        .expect("\"CARGO_MANIFEST_DIR\" must be set")
        .into();

    let config_path = manifest_dir.join("encre-css.toml");
    let src_path = &manifest_dir.join("src");
    let components_path = src_path.join("components");
    let pages_path = src_path.join("pages");
    let static_path = manifest_dir.join("static");
    let public_path = manifest_dir.join("public");

    // Copy all files from "static" to "public"
    fs::remove_dir_all("public").ok();
    fs::create_dir("public").unwrap();
    copy_dir(&static_path, &public_path);

    let mut config = encre_css::Config::from_file(&config_path).unwrap();

    // Register any encre-css plugins here
    encre_css_icons::register(&mut config);

    // Build HTML and CSS files
    build_html(&pages_path, &public_path, &components_path, &config);

    println!("cargo:rerun-if-changed={}", config_path.display());
    println!("cargo:rerun-if-changed={}", src_path.display());
    println!("cargo:rerun-if-changed={}", static_path.display());
}
